$(document).ready(function(){
    $('.multiple-items').slick({
        infinite: true, 
        slidesToShow: 4, 
         slidesToScroll: 4, 
         arrows: true, 
         dots: true, 
         dotsClass: 'slick-dots', 
         responsive: [{
             breakpoint: 600, 
             settings: {
                 slidesToShow: 2, 
                 slidesToScroll: 2, 
             }
         }]
    });      
});